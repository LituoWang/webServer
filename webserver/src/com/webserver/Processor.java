/**
 * 
 */
package com.webserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

/**
 * @author Lituo Wang
 */
public class Processor extends Thread {

	private Socket socket;
	private InputStream in;
	private PrintStream out;
	public final static String WEB_ROOT = System.getProperty("user.dir") + "//resources";

	Processor() {
	}

	Processor(Socket scoket) {
		this.socket = scoket;
		try {
			in = socket.getInputStream();
			out = new PrintStream(socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		String fuleName = parse(in);
		sendFile(fuleName);
	}

	public String parse(InputStream in) {
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String fileName = null;
		try {
			String httpMessage = br.readLine();
			String[] content = httpMessage.split(" ");
			if (content.length != 3) {
				this.sendErrorMessage(400, "client query error!");
				return null;
			}
			System.out.println("code:" + content[0] + ",filename" + content[1] + ",http version:"
					+ content[2]);
			fileName = content[1];
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileName;
	}

	public void sendErrorMessage(int errorCode, String errorMessage) {
		out.println("HTTP/1.0 " + errorCode + " " + errorMessage);
		out.println("content-type: text/html");
		out.println();
		out.println("<html>");
		out.println("<title>Error Message");
		out.println("</title>");
		out.println("<body>");
		out.println("<h1>ErrorCode:" + errorCode + ",Message:" + errorMessage + "</h1>");
		out.println("<body>");
		out.println("</body>");
		out.println("</html>");
		out.flush();
		out.close();
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendFile(String fileName) {
		File file = new File(this.WEB_ROOT + fileName);
		if (!file.exists()) {
			sendErrorMessage(404, "File Not Found!");
			return;
		}
		try {
			InputStream in = new FileInputStream(file);
			byte content[] = new byte[(int) file.length()];
			in.read(content);
			out.println("HTTP/1.0 200 queryfile");
			out.println("content-length:" + content.length);
			out.println();
			out.write(content);
			out.flush();
			out.close();
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
